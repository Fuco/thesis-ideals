 \chapter{Prime ideals and integral elements}
\label{chap:prime-ideals}

% TODO: zmenit mnoziny S na X!! (S je ring)

This chapter will introduce the basic ``building blocks'' on which we will build much of the later-exposed material.  In the presentation we will try to stay self-contained \WRT to ideas developed in this chapter, but will occasionally refer to outside sources when the proof shall carry us away from the stream of ideas.  Some basic results from ring theory will be assumed to be generally known and not acknowledged explicitly.

We will first present some results about prime ideals, then introduce the notion of integral elements, which generalize algebraic elements from field theory.

\section{Prime Ideals}
\label{sec:prime-ideals}

Before we get to prime ideals, let us first recall another important class of rings.  The theorem following the definition is a basic result in ring theory, but we chose to list it for its oft use.

\begin{definition}\label{def:ideal-maximal}\index[terms]{ideal!maximal}
  Let $R$ be a ring.  An ideal $M \subseteq R$\index[symbols]{subseteq@$A \subseteq B$: $A$ is subset of $B$, possibly $A = B$} is called \emph{maximal} if $M \neq R$ and there is no ideal $N$ such that $M \subsetneq N \subsetneq R$\index[symbols]{subset@$A \subsetneq B$: $A$ is strict subset of $B$}.  That is, the only ideal containing $M$ is $R$ and $M$ itself.
\end{definition}

The promised theorem ties together maximal ideals and their resulting quotient rings.

\begin{theorem}[7.4.12 in~\cite{dummit1999abstract}]\label{th:quotient-by-max-field}
  Let $R$ be a (commutative) ring, $M$ its ideal.  Then $M$ is \emph{maximal} \IFF $R / M$\index[symbols]{factorization@$R/I$: factor-ring or module} is a field.
\end{theorem}

\begin{definition}\label{def:ideal-prime}\index[terms]{ideal!prime}
  Let $R$ be a (commutative) ring, let $I$ be an ideal in $R$.  We say that $I$ is \emph{prime} if $I \subsetneq R$ and whenever $ab \in I$, then $a \in I$ or $b \in I$.
\end{definition}

The definition of prime ideal might at first sight appear quite arbitrary.  However, as the name suggests, it is a generalization of the notion of prime numbers from the ring of integers $\ZB$\index[symbols]{aaaintegers@$\ZB$: ring of integers}, so let us first consider the situation there.  First, the condition $I \subsetneq R$ is an analogy of the convention that $1$ is not a prime number---the ideal generated by $1$ is the entire ring.  Further, the condition about membership is equivalent to one about divisibility:
\begin{align}
n \divides ab \Rightarrow n \divides a \vee n \divides b. \label{eq:divisibility-definition-of-ideal}
\end{align}
To verify this, recall that all ideals in $\ZB$ are of the form $(n); n \in \ZB$\index[symbols]{$(r)$: principal ideal generated by $r$}, so $a \in (n)$ means $a = nk; k \in \ZB$.  But the condition~\eqref{eq:divisibility-definition-of-ideal}, known as \emph{Euclid's Lemma}, holds exactly when $n$ is prime number.

A direct generalization of prime numbers are \emph{prime elements}.

\begin{definition}\label{def:principal-prime}\index[terms]{prime element}\index[terms]{element!prime}
  Let $R$ be a ring, $p$ a non-unit of $R$.  We say that $p$ is \emph{prime element} if $(p)$ is non-zero prime ideal.
\end{definition}

Notice that the definition of prime element can be equivalently said in terms of~\eqref{eq:divisibility-definition-of-ideal}.  In PIDs\footnote{\emph{Principal Ideal Domain}, an \ID where every ideal is principal.}\index[terms]{domain!of principal ideals} (which $\ZB$ is) every element can be uniquely factored into prime elements (see Section 8.3 in~\cite{dummit1999abstract}).

% In PIDs, ideals can also model greatest common divisors: if $d = \gcd(a,b)$, then the ideal $(a,b)$ generated by $a$ and $b$ is $(d)$, which explains why $(a,b)$ is often used to denote GCD.

The following theorem ties together prime ideals and their resulting quotient rings, in analogy to~\cref{th:quotient-by-max-field}.

\begin{theorem}[7.4.13 in~\cite{dummit1999abstract}]\label{th:quotient-by-prime-id}
  Let $R$ be a ring, $I$ its ideal.  Then $I$ a \emph{prime ideal} \IFF $R / I$ is \emph{\ID}.
\end{theorem}

% \begin{proof}
% Let $I$ be prime. Because $R$ is commutative, so is $R / I$.  We wish to show that $R / I$ has no zero divisors.

% Suppose that $(a + I) (b + I) = ab + I = 0 + I$.  Then $ab - 0 \in I \Rightarrow ab \in I$.  But $I$ is prime and so (without loss of generality) we can assume $a \in I$. But then $a + I = I$.

% Conversely, suppose $R / I$ is an \ID, and let $a, b \in R$ such that $ab \in I$.  Then $0 + I = I = ab + I = (a + I) (b + I)$.
% Because $R / I$ is \ID, $(a + I)$ or $(b + I)$ is zero. Let for example $a + I$ be zero, then $a \in I$ and so $I$ is prime.
% \end{proof}

Since $R \cong R / 0$\index[symbols]{$\cong$: isomorphism of two structures}, we immediately have the following:

\begin{corollary}\index[terms]{domain!integral}
  Let $R$ be a ring.  Then $R$ is \ID \IFF $0$ is prime ideal in $R$.
\end{corollary}

\begin{corollary}\label{cor:every-maximal-is-prime}
  Every maximal ideal is prime, because every field is \ID.
\end{corollary}

We will now follow the definition with some characteristics of prime ideals.

First, we consider prime ideals from the point of view of their set-theoretic complements\footnote{Unless stated otherwise, we will always mean \emph{set-theoretic complement} when we say \emph{complement} from now on.}.  We recall that a \emph{\MCS}\index[terms]{set!multiplicatively closed} is a set $X$ where $a, b \in X$ imply $ab \in X$.\footnote{Bourbaki has also introduced the term \emph{magma} for this structure.  Additionally, these are sometimes called \emph{groupoids}, but beware as this term has also a very different meaning in context of Category Theory.}

\begin{theorem}\label{th:ideal-prime-compl}
Let $I$ be an ideal in $R$ an let $X$ be its complement.  Then $I$ is prime \IFF $X$ is multiplicatively closed.
\end{theorem}

\begin{proof}
If $I$ is prime, then $a, b \not\in I$ ($a,b \in X$) imply $ab \not\in I$, and so $ab \in X$.
Conversely, let $ab \in I$. Then $ab \not\in X$ and so at least one of $a$ and $b$ must not lie in $X$, therefore at least one must lie in $I$.
\end{proof}

It follows directly from definition of $X$ that $I$ is the maximal ideal \WRT exclusion from $X$.  The following theorem shows that converse also holds.

% Theorem 1
\begin{theorem}[1 in~\cite{kaplansky1974commutative}]\label{th:maximal-mcs-prime}
  Let $X$ be a \MCS in a ring $R$, let $I$ be an ideal in $R$ which is maximal \WRT exclusion from $X$.  Then $I$ is prime.
\end{theorem}

\begin{proof}
  Let $a,b \in R$.  Given that $ab \in I$, we wish to show that $a \in I$ or $b \in I$.  Suppose for contradiction that neither $a$ nor $b$ lies in $I$.  Then the ideal $(I,a)$ is strictly larger than $I$ since it at least contains additional element $a$.  Because $I$ is maximal \WRT exclusion from $X$, $X \cap (I,a)$ is non-empty.  Therefore there exists an element $x_1 \in X \cap (I,a)$ of the form $x_1 = i_1 + ra, r \in R$.  Similarly with $(I,b)$ we get $x_2 = i_2 + sb$.  Then
\[
x_1x_2 = (i_1 + ra)(i_2 + sb) = i_1i_2 + i_1sb + rai_2 + rsab
\]
and all four terms lie in $I$---the first three because $i_1$ and $i_2$ lie in $I$, and the last one because $ab \in I$ by assumption.  So $x_1x_2$ lies in $I$, hence $x_1x_2 \not\in X$, a contradiction with $X$ being multiplicatively closed.
\end{proof}

Let us for a moment inspect more closely the complement $X$ of an ideal $I$.  Let $a$ be a divisor of $x \in X$, that is $x = ar, r \in R$.  Now $a$ must also lie in $X$, for if not, $a \in I$ would imply that $ar \in I$ and therefore $x \in I$, which is a contradiction.  We see that the complement of an ideal is \emph{saturated} with the divisors of every of its elements.  We summarize the notion in the following definition.

\begin{definition}\index[terms]{set!saturated}
  We say that a \MCS $X$ is \emph{saturated} if with every $x \in X$ it also contains all the divisors of $x$.
\end{definition}

Using the notion of a saturated set we can now sharpen~\cref{th:maximal-mcs-prime}.

% Theorem 2
\begin{theorem}[2 in~\cite{kaplansky1974commutative}]\label{th:saturated-complement-prime}
  Let $R$ be a ring and $X$ its subset.  Then $X$ is saturated \MCS \IFF the complement of $X$ is set-theoretic union\footnote{Unless stated otherwise, we will always mean \emph{set-theoretic union} when we say \emph{union} from now on.} of prime ideals in $R$.
\end{theorem}

\begin{proof}
Let $X$ be saturated \MCS.  Let $a$ be any element in the complement of $X$.  Because $X$ is saturated, the principal ideal $(a)$ is disjoint from $X$, for if $ar \in X; r \in R$, then $a$ must also be in $X$ since it is a divisor of $ar$, a contradiction.  We can expand $(a)$ to an ideal $I$ maximal \WRT exclusion from $X$ (this is possible by Zorn's lemma, see Appendix~\ref{app:zorn}). By~\cref{th:maximal-mcs-prime} $I$ is prime.  Since the choice of $a$ was arbitrary, it follows that every element from complement of $X$ lies in some prime ideal disjoint from $X$.  The other direction is trivial (take any $a,b \in X$, observe what happens if $ab \not\in X$).
\end{proof}

\begin{remark}
  Note that in the union of prime ideals in preceding theorem it is enough to consider those among them which are maximal (\WRT inclusion).
\end{remark}

Before we move further, here are some examples of multiplicative and saturated sets and the use of previous theorem.

\begin{example}\label{example:r-to-n}
  Let $r \in R$ be non-zero.  Then the set $X = \{r^n\}_{n=1}^\infty$ is multiplicatively closed, $r^mr^n = r^{m+n} \in X$.  This set in general is not saturated.  For example, in $\ZB/10\ZB$, take $r = 2$, then $4 = 2^2 = 3 \cdot 8$, so $3|4$, but $3 \not\in X$.  Conversely, if we take $R$ to be a principal ideal domain and pick a prime element $r$, the set $X$ is saturated (because the factorization into primes is unique).  The set ``powers of $r$'' is encountered very often and will play a key role later (see~\cref{th:nilradical-intersec-g-ideals}).
\end{example}

\begin{example}\label{example:non-units}
The set $\{1_R\}$ is multiplicatively closed.  If we take its saturation we get the set of all units of $R$ (we will denote this set $\units{R}$\index[symbols]{R@$\units{R}$: set of units}).  The prime ideals maximal in its complement are just the ordinary maximal ideals.  Indeed, recall that the set of all non-units is precisely the union of maximal ideals\footnote{Any non-unit $x$ generates ideal $(x)$ which lies in some maximal ideal, see~\cref{th:minimal-prime-exist}.}, which exactly corresponds with the previous theorem.
\end{example}

\begin{example}\label{example:maximal-primes}
Let $X$ be the set of all non-zero-divisors in $R$.  It is a simple exercise to show that $X$ is saturated and multiplicatively closed.  Therefore, by~\cref{th:saturated-complement-prime}, the set of all zero-divisors of $R$ is a union of prime ideals.  We will look at zero-divisors in more detail in~\autoref{chap:noetherian-rings}.
\end{example}

We now come back to more theorems which in a way tell us how we can ``construct\footnote{These theorems often rely on Axiom of Choice or equivalent theorems, so they are properly speaking non-constructive.  Better word would maybe be ``exhibit''.}'' prime ideals.  An interesting recurring feature is that when we take some set or property and an ideal \emph{maximal} \WRT this set or property, it often turns out to be prime.  When we inspect the proofs of the three theorems of this kind in this thesis, there does not seem to be an obvious connection.  The work presented in~\cite{okaako} summarizes properties of certain families of ideals, such that an ideal maximal \WRT \emph{not} being a member of this family is then prime.

Before we formulate the next theorem, let us recall the definition of an \emph{annihilator} of a set.

\begin{definition}\index[terms]{annihilator}
  Let $R$ be a ring, $A$ an $R$-module and $B$ a nonempty subset of $A$.  The \emph{annihilator} of $B$ in $R$, denoted $\Ann_R(B)$\index[symbols]{annihilator@$\Ann_R(A)$: annihil. of $R$-module $A$}, is the set of all elements $r$ in $R$ such that for each $b$ in $B$, $rb = 0$. In set notation,
\[
\Ann_R(B)=\{r \in R \mid \forall b \in B, rb=0 \}.
\]
To say that an element $r$ annihilates $B$, we also write $rB = 0$ instead of $r \in \Ann_R(B)$.
\end{definition}

The next theorem tells us how we can find a prime ideal by looking at a special type of annihilators.  We leave as a simple exercise to verify that an annihilator really forms an ideal\footnote{Use the distributive and associative laws from definition of a module on two elements from the annihilator.}.

% Theorem 6
\begin{theorem}[Herstein, 6 in~\cite{kaplansky1974commutative}]\label{th:maximal-ann-prime}
  Let $R$ be a ring and $A$ an $R$-module.  Let $I = \Ann_R(x)$\index[symbols]{annihilator@$\Ann_R(x)$: annihil. of set $\{x\}$} be an ideal in $R$ that is maximal among all annihilators of non-zero elements $x$ of $A$.  Then $I$ is prime.
\end{theorem}

\begin{proof}
Let $I$ be the annihilator of $x \in A$.  We wish to show that given $ab \in I$, $a$ or $b$ lies in $I$.  If $a \in I$, we are done.  Assume that $a \not\in I$.  Then $ax \neq 0$ (by definition of $I$).  When we consider $u \in I$, we see that
\[
u(ax) \overset{com.} = a(ux) = a0 = 0
\]
so $u \in \Ann(ax)$ and $I \subseteq \Ann(ax)$.  By hypothesis $I$ is maximal and so $I = \Ann(ax)$.  Because $ab \in I$ implies $0 = (ab)x = b(ax)$, $b$ annihilates $ax$, hence $b \in I$.
\end{proof}

% Theorem 7
\begin{theorem}[Cohen, \cite{cohen1950}, 7 in~\cite{kaplansky1974commutative}]\label{th:maximal-non-fin-gen-prime}
  Let $I$ be an ideal in ring $R$.  Suppose $I$ is not finitely generated and is maximal among all ideals in $R$ that are not finitely generated.  Then $I$ is prime.
\end{theorem}

\begin{proof}
  Assume for contradiction that $ab \in I$ and neither $a$ nor $b$ are in $I$.  Then the ideal $(I,a)$ is properly larger than $I$ and hence finitely generated, let's say by elements $i_k + x_ka, k \in \{1,\ldots,n\}, i_k \in I, x_k \in R$.  Further, let $J = \{y \in R \mid ay \in I\}$.  Because $aI \subseteq I$, we get $I \subseteq J$.  Further, from $ab \in I$ we get $b \in J$.  Because $J$ contains both $I$ and $b$ it is properly larger than $I$ and so finitely generated.  Denote
\[
X = (i_1, \ldots, i_n, aJ).
\]
This ideal is finitely generated.  We will now show that $I = X$, which will be a contradiction with $I$ not being finitely generated.  For $I \subseteq X$, take any $x \in I$.  Then \emph{a fortiori} $x \in (I,a)$, so we get
\begin{align*}
x = \sum_{j=1}^{n}r_j(i_j + x_ja) = \sum_{j=1}^{n} r_ji_j + a\sum_{j=1}^{n}r_jx_j
\end{align*}
where $r_j \in R$.  Now we see that $a\sum_{j=1}^{n}r_jx_j \in I$ and so $\sum_{j=1}^{n}r_jx_j \in J$, hence $i \in (i_1, \ldots, i_n, aJ)$ as required.  Conversely, any $x \in X$ can be written as
\[
x = \sum_{j=1}^{n} r_ji_j + ay,
\]
where $y \in R$.  This means $x$ lies in $I$.  Hence $I = X$ is finitely generated and we get the desired contradiction.
\end{proof}

The theorem also holds if we replace the non-finitely generated ideals by non-principal ones.

\begin{theorem}
  Let $I$ be an ideal in ring $R$.  Suppose $I$ is not principal and is maximal among all ideals in $R$ that are not principal.  Then $I$ is prime.
\end{theorem}

\begin{proof}
  The proof is analogous to that of~\cref{th:maximal-non-fin-gen-prime}.
\end{proof}

Before moving to the next topic, we insert a theorem best fitting in this chapter, although of a different sort than the previous ones.

% Theorem 10
\begin{theorem}[10 in~\cite{kaplansky1974commutative}]\label{th:minimal-prime-exist}\index[terms]{ideal!minimal prime}
  Let $R$ be a ring, $I \subsetneq R$ a proper ideal.  Then there is a minimal prime ideal (\WRT set inclusion) above it.
\end{theorem}

\begin{proof}
  To show the existence of the minimal prime, we will use Zorn's lemma ``going downwards''\footnote{It is the same as usual application in the opposite lattice.}.  Therefore, we verify the conditions.  The set of prime ideals over $I$ is non-empty, because $I$ is contained in some maximal ideal $M$ (7.4.11 in~\cite{dummit1999abstract}), which is prime (\cref{cor:every-maximal-is-prime}).  Now we need to show that any decreasing chain $\{P_i\}$ of prime ideals has a lower bound.  But taking $\bigcap_i P_i$ we get a prime ideal under all of the $P_i$.  By Zorn's lemma there is a minimal prime over $I$.
\end{proof}

\section{Integral Elements}
\label{sec:integral-elements}

In this section we start developing theory of integral elements, which are a generalization of algebraic elements from field theory.  We will develop the basic theory over \emph{associative algebras}, which are modules with an additional ``multiplication'' operation.  It can be equally well developed over rings, but as we will see, every algebra is a ring so it makes the theory a bit more general.

% After some general theorems, we will move to study commutative rings and introduce valuation domains, invertible ideals and Pr\"ufer domains.  These will be later used in~\autoref{sec:zero-divisors}.

\begin{definition}\index[terms]{R-Algebra@$R$-Algebra}\index[terms]{associative algebra}
  Let $R$ be a commutative ring.  An \emph{associative \ALG{R}} $A$ is an $R$-module with a binary operation $[\cdot,\cdot] : A \times A \rightarrow A$ which satisfies
\[
[rx + sy,z] = r[x,z] + s[y,z], ~~~ [z,rx + sy] = r[z,x] + s[z,y]
\]
for all $r,s \in R$ and $x,y,z \in A$ (that is, $[\cdot,\cdot]$ is \emph{bilinear}).  To simplify the notation, we will usually write $xy$ instead of $[x,y]$.
\end{definition}

Note that the operation $[\cdot,\cdot]$ also makes $A$ into a ring.  In fact, it is easy to see that every ring $R$ is in this way an \ALG{R} over itself (define $[\cdot,\cdot]$ as the usual ring multiplication).

The following is a generalization of \emph{algebraic elements} from field theory.

\begin{definition}\index[terms]{element!integral}\index[terms]{element!algebraic}
  Let $R$ be a commutative ring and $A$ an \ALG{R}.  We say that element $a \in A$ is \emph{integral} over $R$ if there exists a polynomial $p \in R[x]$\index[symbols]{$R[x]$: ring of polynomials over $R$} whose leading coefficient is $1$ such that $p(a) = 0$.  Explicitly, that is
\begin{align}
  a^n + r_{n-1}a^{n-1} + \ldots + r_1a + r_0 = 0 ~~~(n \in \NB\index[symbols]{aaanaturals@$\NB$: set of natural numbers}). \label{eq:integral-elem-poly}
\end{align}
We say that algebra $A$ is \emph{integral}\index[terms]{algebra!integral over $R$} over $R$ if all of its elements are integral over $R$.  We say that an element $a$ is \emph{algebraic} over $R$ if the coefficient at the leading term $a^n$ in~\eqref{eq:integral-elem-poly} is allowed to be other than $1$.  Analogically, an algebra $A$ is \emph{algebraic}\index[terms]{algebra!algebraic over $R$} over $R$ if all of its elements are algebraic over $R$.
\end{definition}

The following lemma gives us a necessary and sufficient condition on an element $a$ being integral.

% Theorem 12
\begin{lemma}[12 in~\cite{kaplansky1974commutative}]\label{lem:integral-elem-submod}
  Let $R$ be a commutative ring, $A$ an \ALG{R}, $a \in A$.  Then $a$ is integral over $R$ \IFF there exists a finitely generated $R$-submodule $B$ of $A$ such that $aB \subseteq B$ and the left annihilator of $B$ in $A$ is~$0$.
\end{lemma}

\begin{proof}
  First, let $a$ be integral.  Then there exists a polynomial $p$ satisfying~\eqref{eq:integral-elem-poly}.  Let $B = \langle 1,a,a^2,\ldots,a^{n-1} \rangle$.  Given any $b \in B$ we get:
\begin{align*}
b &= r_{n-1}a^{n-1} + r_{n-2}a^{n-2} + \ldots + r_1a + r_01 \\
ab &=  r_{n-1}a^{n} + r_{n-2}a^{n-1} + \ldots + r_1a^2 + r_0a \\
ab &= r_{n-1}p(a) + \xi
\end{align*}
where $\xi$ contains only powers of $a$ smaller than $n$ and so lies in $B$.  Therefore $ab \in B$ because $r_{n-1}p(a) = 0$.  To show that the left annihilator of $B$ in $A$ is $0$, let us suppose that there is $x \in A$ such that $xB = 0$.  Then in particular
\[
x = [x,1] \overset{1 \in B}= 0.
\]
In the other direction, let $B = \langle b_1,\ldots,b_n \rangle, n \in \NB$.  From the assumption $aB \subseteq B$, we get
\begin{align*}
  ab_i &= \sum_{j=1}^n \lambda_{ij}b_j ~~~ (\lambda_{ij} \in R; i = 1,\ldots,n) \\
  ab_i - \sum_{j=1}^n \lambda_{ij}b_j &= -\lambda_{i1}b_1 - \lambda_{i2}b_2 - \ldots + (a - \lambda_{ii})b_i - \ldots - \lambda_{in}b_n= 0
\end{align*}
If we let $b = (b_1\ldots,b_n)^T$ and $\Lambda$ be a matrix formed by the coefficients $\delta_{ij}a - \lambda_{ij}$\footnote{$\delta_{ij}$ known as \emph{Kronecker delta} (after Leopold Kronecker) is a function of two variables $i, j$ that has a value of $1$ if $i = j$ and $0$ otherwise.}\index[symbols]{$\delta_{ij}$:Kroneker delta}, we can express the above equations as $\Lambda b = 0$.  By Cramer's Rule (11.4.26 in~\cite{dummit1999abstract}) we get $(\det\Lambda) b_i = 0$\index[symbols]{$\det A$: determinant of matrix $A$}.  Because the annihilator of $B$ is 0 and $\det\Lambda$ annihilates all the generators, $\det\Lambda$ must be $0$.  We get the required polynomial for $a$ by expanding the determinant.
\end{proof}

\begin{remark}
  In the formulation of~\cref{lem:integral-elem-submod}, we can replace the condition $\Ann_A(B) = 0$ by the condition that $1 \in B$.  Then, using the notation of the preceding proof, we wish to show that $\det \Lambda = 0$.  Because $\det \Lambda$ is an annihilator of $B$, we get
\[
\det \Lambda = [\det\Lambda,1] \overset{1 \in B} = 0.
\]
Conversely, we can always pick $B$ such that it contains $1$ as shown in the ``$\Rightarrow$'' direction of the proof.
\end{remark}

We can use the above lemma at once to prove that integral elements behave ``nicely'' under the $R$-algebra operations.

% Theorem 13
\begin{theorem}[13 in~\cite{kaplansky1974commutative}]\label{th:sumprod-is-integral}
  Let $R$ be a commutative ring, $A$ an \ALG{R} and $b,c \in A$ integral elements over $R$ which commute with all elements of $A$.  Then $b + c, bc$ are also integral over $R$.
\end{theorem}

\begin{proof}
  Let $B$ and $C$ be submodules of $A$ satisfying the conditions of~\cref{lem:integral-elem-submod} for $b$ and $c$.  Let us further suppose that $1 \in B,C$ (which we can by the remark above).  Then, if we show that the submodule product $BC$ satisfies both $(b + c)BC \subseteq BC$ and $(bc)BC \subseteq BC$, by applying~\cref{lem:integral-elem-submod} we get the result.   We show the former case, the latter is analogous.  Let $x \in BC$, then
\begin{align*}
x &= \sum_i^n b_ic_i \\
(b + c)x &= \sum_i^n (b + c)b_ic_i \\
            &= \sum_i^n \underbrace{bb_i}_{\in B}c_i + b_i\underbrace{cc_i}_{\in C}
\end{align*}
which is again a finite sum of products from $B$ and $C$.
\end{proof}

% Theorem 14
\begin{corollary}[14 in~\cite{kaplansky1974commutative}]\label{cor:integral-elements-form-subring}
  Let $R$ be a commutative ring and $A$ a commutative \ALG{R}.  Then the elements of $A$ integral over $R$ form a subring of $A$.
\end{corollary}

We can further prove a similar result about invertible algebraic elements.

\begin{theorem}\label{th:inverse-is-algebraic}
  Let $R \subseteq S$ be {\ID}s, $u \in S$ invertible.  Then if $u$ is algebraic over $R$, so is $u^{-1}$.
\end{theorem}

\begin{proof}
  We will directly construct the polynomial which annihilates $u^{-1}$.  Let $p \in R[x]$ be polynomial of minimal degree (we can always pick one) such that $p(u) = 0$.  Then
\begin{align*}
  0 &= \polynom{u}{r}{n}\\
    &= u^n(r_n + r_{n-1}u^{-1} + \ldots + r_1u^{1-n} + r_0u^{-n}).
\end{align*}
Now, if $u^n$ is $0$, then $p(x)$ is not minimal.  Because $S$ is \ID, it follows that the other factor must be $0$, therefore the polynomial
\[
r_0x^n + r_1x^{n-1} + \ldots + r_{n-1}x + r_n
\]
has $u^{-1}$ as a root.
\end{proof}

The following lemma will be used to prove~\cref{th:field-integral}.

% Theorem 15
\begin{lemma}[15 in~\cite{kaplansky1974commutative}]\label{lem:invertible-integral}
  Let $R, S$ be commutative rings such that $R \subseteq S$, let $s \in S$ be invertible in $S$.  Then $s^{-1}$ is integral over $R$ \IFF $s^{-1} \in R[s]$.
\end{lemma}

\begin{proof}
  If $s^{-1}$ is integral over $R$, we have:
  \begin{align}
    s^{-n} + r_{n-1}s^{-(n-1)} + \ldots + r_1s^{-1} + r_0 &= 0 \notag \\
    s(r_{n-1} + r_{n-2}s + \ldots + r_1s^{n-2} + r_0s^{n-1}) &= -1\notag \\
    - (r_{n-1} + r_{n-2}s + \ldots + r_1s^{n-2} + r_0s^{n-1}) &= s^{-1}. \label{eq:15.1}
  \end{align}
The left-hand side of~\eqref{eq:15.1} lies in $R[s]$ by definition, and so $s^{-1} \in R[s]$.  To prove the other direction we simply reverse the argument.
\end{proof}

% Theorem 16
\begin{theorem}[16 in~\cite{kaplansky1974commutative}]\label{th:field-integral}
  Let $R$ be an \ID contained in a field $S$.  If $S$ is integral over $R$ then $R$ is a field.
\end{theorem}

\begin{proof}
  Let $0 \neq r \in R$.  We wish to show that $r^{-1} \in R$.  We know that $r^{-1} \in S$ because $S$ is a field.  By assumption $r^{-1}$ is integral over $R$ and by~\cref{lem:invertible-integral} $r^{-1} \in R[r] = R$.  It follows that $R$ is a field.
\end{proof}

Before moving to a different topic, we conclude with a note on finitely generated \emph{rings} as opposed to finitely generated \emph{modules}.  The essential difference is that when generating as a ring, we can make use of both the ring sum and product, whereas with $R$-modules we are essentially making \emph{linear combinations} of the generating set\footnote{Be aware that unlike vector spaces, not all modules have a basis, and can even possess independent generating sets of different cardinalities.} with coefficients in $R$.  The ring generation is captured by the bracket notation: $R[a_1,\ldots,a_n,\ldots]$\index[symbols]{$R[s]$: ring formed by adjoining $s$ to ring $R$} means ``add elements $a_1,\ldots,a_n,\ldots$ to $R$ and then close it under the ring operations''.  The module generation is noted similarly to linear spans with $\langle a_1,\ldots,a_n,\ldots \rangle$.

To adduce an example, $R[x]$ is finitely generated \emph{as a ring} over $R$ by the set $\{x\}$---in other words, we freely sum and multiply the elements of the set $R \cup \{x\}$.  However, it is not finitely generated as a module---the generating set is $\langle 1, x, x^2,\ldots,x^n,\ldots\rangle$\index[symbols]{$\langle x \rangle$: module generated by $x$}.

The following theorem provides a connection between the two.

% Theorem 17
\begin{theorem}[17 in~\cite{kaplansky1974commutative}]\label{th:finitely-gen-mod-finitely-gen-ring-integral}
  Let $R$ be a ring, $A$ an \ALG{R}.  Then the following statements are equivalent:
  \begin{enumerate}
  \item $A$ is finitely generated $R$-module.
  \item $A$ is finitely generated ring over $R$ and is integral over $R$.
  \end{enumerate}
\end{theorem}

\begin{proof}
(i) $\Rightarrow$ (ii): Taking $A$ for $B$ in~\cref{lem:integral-elem-submod} gives us that $A$ is integral over $R$.  That it is finitely generated as a ring is obvious.

(ii) $\Rightarrow$ (i): Assume that $A$ is generated as a ring over $R$ by $a_1,\ldots,a_n$.  Suppose further that the degree of the polynomial $p_i$ showing $a_i$ being integral over $R$ is $n_i$.  This means that whenever we have an element $a_i^m$ where $m > n_i$, we can reduce it via the polynomial $p_i$ to an expression with all powers of $a_i$ smaller than $n_i$.  Then all the products of the form
\[
a_1^{r_1}a_2^{r_2}\cdots a_n^{r_n}; ~~~ r_i = 0,1,\ldots,n-1
\]
generate $A$ as a module over $R$.
\end{proof}

% From now on we will lose the generality of modules and algebras and continue with commutative rings $R \subseteq S$.

% \begin{definition}
%   Let $R \subseteq S$ be rings.  The elements of $S$ integral over $R$ form a subring of $S$ (cf. \cref{cor:integral-elements-form-subring}).  This subring is called \emph{integral closure} of $R$ in $S$.  An \emph{integrally closed domain} is an \ID $R$ whose integral closure in its \QF is $R$ itself.
% \end{definition}

% The property of an element being integral is transitive in the following sense.

% % Theorem 40
% \begin{theorem}
%   Let $R \subseteq S \subseteq T$ be rings, let $t \in T$ be integral over $S$ and $S$ be integral over $R$.  Then $t$ is integral over $R$.
% \end{theorem}

% \begin{proof}
%   Let
% \[
% p = \polynomi{x}{s}{n}; ~~~ s_i \in S
% \]
% be the polynomial exhibiting the fact that $t$ is integral over $S$, i.e. $p(t) = 0$.  Let $\tilde{R} = R[s_1,\ldots,s_n,t]$.  It is relatively straight-forward to show that $\tilde{R}$ can be finitely generated as a module over $R$.\footnote{Consider adding sufficiently many powers of $s_1,\ldots,s_n$ and $t$---it is enough to add finitely many because over certain $n_i$ we can rewrite them via the polynomials exhibiting integrality to be of at most $(n_i-1)$th power.  The argument is essentially the same as the (ii) $\Rightarrow$ (i) part of the proof of~\cref{th:finitely-gen-mod-finitely-gen-ring-integral}.}  By~\cref{th:finitely-gen-mod-finitely-gen-ring-integral} $\tilde{R}$ is integral over $R$ and the more so $u$ is integral over $R$.
% \end{proof}



% \begin{lemma}\label{lem:integral-elem-submod}
%   Let $R$ be a commutative ring, $A$ an \ALG{R}, $a \in A$.  Then $a$ is integral over $R$ \IFF there exists a finitely generated $R$-submodule $B$ of $A$ such that $aB \subseteq B$ and the left annihilator of $B$ in $A$ is $0$.
% \end{lemma}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: