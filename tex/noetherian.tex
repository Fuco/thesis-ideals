\chapter{Zero divisors in Noetherian settings}
\label{chap:noetherian-rings}

% TODO: zmenit mnoziny S na X!! (S je ring)

It is usually the case that---in some sense---``finite'' objects are easier to classify and study than their ``infinite'' counterparts.  Restricting ourselves to study of finite rings, however, would take away too much freedom and the theorems would end up being rather dull.  For example, all finite integral domains are automatically fields\footnote{This is know as \emph{Wedderburn's little theorem}, see section 13.6., exercise 13 in~\cite{dummit1999abstract} for outline of a proof.} and therefore have all the ``nice properties'' that we could imagine.

A weaker notion is the one where the rings are \emph{\FG}.  This however, is too weak, because it does not provide much information about the ideal structure---an ideal of \FG ring is not necessarily \FG itself, see the example preceding~\cref{th:noetherian-r-finite-module-noetherian}.

We will therefore settle for the notion situated somewhere between these two extremes: we will require that every ideal of the ring be \FG.

In the first part of the chapter we will present some properties of rings and modules satisfying the above property.  We will then present some results about zero-divisors in such modules.  Finally, we introduce the notions of \emph{maximal} and \emph{minimal} primes and show basic properties about their structure.

We first summarize the condition in a definition, providing also an alternative useful characterization.

\begin{definition}\index{terms}{ring!Noetherian}
  A ring $R$ is called \emph{Noetherian} if any (and therefore all) of the following holds:
  \begin{enumerate}
  \item Every ideal in $R$ is \FG.
  \item Ideals in $R$ satisfy the ascending chain condition (ACC), that is, every strictly ascending chain of ideals $I_1 \subsetneq I_2 \subsetneq \ldots$ eventually terminates.
  \end{enumerate}
\end{definition}

We say that the chain \emph{stabilizes} at (index) $m$ if $I_m = I_{m+1} = I_{m+2} = \ldots$, where $m$ is the least such index.

Here is another characterizations of {\NR}s.

% Theorem 8
\begin{theorem}
  Let $R$ be a ring.  If every prime ideal in $R$ is \FG, then $R$ is Noetherian.
\end{theorem}

\begin{proof}
  Let us prove the contrapositive: if there is a non-\FG ideal $I$ in $R$, then there is a non-\FG prime ideal in $R$.  We can enlarge $I$ (Zorn) to an ideal maximal among non-\FG ones.  Then $I$ is prime by~\autoref{th:maximal-non-fin-gen-prime}.
\end{proof}

% Theorem 69
% \begin{theorem}[Hilbert's basis theorem]
%   If $R$ is a \NR, then so is $R[x]$.
% \end{theorem}

We can naturally extend the Noetherian property onto modules, replacing ideals with submodules.  Notice that if we take $R$ as an $R$-module over itself, then we recover the ring definition from above.

\begin{definition}\index{terms}{module!Noetherian}
  Let $R$ be a ring.  An $R$-module $A$ is Noetherian if any (and therefore all) of the following holds:
  \begin{enumerate}
  \item Every submodule of $A$ is \FG.
  \item Submodules in $A$ satisfy ACC.
  \end{enumerate}
\end{definition}

It is reasonable to expect that the structure of a ring will in some way affect the structure of modules over itself.  The following theorem shows that \FG modules over a \NR must then necessarily also be Noetherian.  This is a useful characterizations, because in general we can not expect that a submodule of \FG module is also \FG.  For example, choose ring $R$ to be $\ZB[x_1,x_2,\ldots,x_n,\ldots]$.  It is finitely generated as an $R$-module by ${1}$, but the submodule (ideal) $I = (x_1,x_2,\ldots,x_n,\ldots)$ is not \FG\footnote{If $I$ is finitely generated, pick any $x_n$ that is not present in any term of the generators, then it can not be expressed.  The details are left as an exercise.}.

Even more trivial example arises from the fact that any \ID is a subring of a field (via the construction of \QF).  Then we only have to take any non-Noetherian \ID---it is a subring of its \QF which is of course Noetherian, the only ideals being the zero ideal and the field itself.

\begin{theorem}[3.1.4 in \cite{berrick2000introduction}]\label{th:noetherian-r-finite-module-noetherian}
  Let $R$ be \NR.  If $A$ is \FG $R$-module, it is Noetherian.
\end{theorem}

\begin{proof}[Proof (sketch)]
Let $S$ be a ring, let
\[
0 \rightarrow M' \rightarrow M \rightarrow M'' \rightarrow 0
\]
be short exact sequence of $S$-modules.  Then $M$ is Noetherian \IFF both $M'$ and $M''$ are.  From this we get that any finite direct sum of $S$-modules is Noetherian.  Since every \FG $S$-module is a homomorphic image of free $S$-module (which is isomorphic to $\bigoplus_n S$ for some $n \in \NB$), and homomorphic image preserves Noetherian property, we get the result by taking $R$ (which is a Noetherian module) for $S$.
\end{proof}

Consider an $R$-module $A$ and an ideal $I \subseteq R$.  Taking the intersection of an infinite chain of ideals $I \subseteq I^2 \subseteq I^3 \subseteq \ldots$ gives us again an ideal, let us denote it $K = \bigcap_{n \in \NB} I^n$.  Now consider the module $KA$.  We might ask when the following ``fix-point'' property holds: $I(KA) = KA$.  \cref{th:krull-intersection-theorem} gives us a sufficient condition, but before we introduce it we state the following technical lemma used in its proof.

% Theorem 73
\begin{lemma}[Herstein]\label{lem:herstein-krull-intersection-theorem}
  Let $R$ be a \NR, $I \subseteq R$ an ideal, $A$ \FG $R$-module, and $B \subseteq A$ a submodule.  Let $C \subseteq A$ be a submodule such that $IB \subseteq C$ and $C$ is maximal \WRT the property $C \cap B = IB$.  Then $I^nA \subseteq C$ for some $n$.
\end{lemma}

\begin{proof}
Since $I$ is \FG, let's say by $x_1,\ldots,x_k$, the elements of $I^n; n \in \NB$ are of the form
\[
\sum_j^{\text{finite}} r_j x_1^{\alpha_{j_1}}\ldots x_k^{\alpha_{j_k}};~~~ \sum_i^k \alpha_{j_i} = n.
\]
If we show that for every $x \in I$ there exists some $n_x \in \NB$ such that $x^{n_x}A \subseteq C$, we can then pick high enough $n$ to ensure that there is always at least one member of the monomial $r_j x_1^{\alpha_{j_1}}\ldots x_k^{\alpha_{j_k}}$ with power higher than $\max_{x \in I} n_x$.  Then for any $a \in A$, the entire term
\[
(r_j x_1^{\alpha_{j_1}}\ldots x_k^{\alpha_{j_k}})a
\]
lies in $C$ by the submodule closure condition on $R$-multiplication.  Consequently, for any $i \in I^n$, we have $iA \subseteq C$, and $I^nA \subseteq C$.  To prove the required property, we proceed as follows.

Fix an $x$ arbitrarily from $I$.  For each $m \in \NB$, let $D_m$ be the submodule of $A$ consisting of all $a \in A$ such that $x^ma \in C$.  These submodules form a chain of inclusions: $D_{m} \subseteq D_{m+1}$, for suppose $a \in D_m$, then
\[
x^ma \in C \Rightarrow x(x^ma) \in C \Rightarrow a \in D_{m+1}.
\]
Because $A$ is Noetherian, the chain $\{D_m\}$ stabilizes at $n_x$.  Now suppose
\begin{align}
  (x^{n_x}A + C) \cap B = IB, \label{eq:lem:herst1}
\end{align}
then by the maximality of $C$ we get $x^{n_x}A \subseteq C$.  We are therefore left to prove~\eqref{eq:lem:herst1}.  The inclusion $\supseteq$ is given by the hypothesis $C \cap B = IB$.  To show $\subseteq$, suppose $t$ is in the left side.  Then
\begin{align}
t = x^{n_x}a + c; ~~~a \in A, c \in C. \label{eq:lem:herst2}
\end{align}
Because $t$ is also in $B$, $xt \in xB \subseteq IB \subseteq C$.  Then
\begin{align*}
xt &= x(x^{n_x}a + c) = (x^{n_x + 1}a + xc) \in C \\
\underbrace{xt}_{\in C} - \underbrace{xc}_{\in C} &= x^{n_x + 1}a
\end{align*}
and so $x^{n_x + 1}a \in C$.  But because $D_{n_x} = D_{n_x + 1}$, we get $x^{n_x}a \in C$, hence $t \in C$ (from~\eqref{eq:lem:herst2}).  Then $t \in (C \cap B) = IB$ and~\eqref{eq:lem:herst1} is proved.
\end{proof}

% Theorem 74
\begin{theorem}[The Krull Intersection Theorem]\label{th:krull-intersection-theorem}
  Let $R$ be a \NR, $I \subseteq R$ an ideal, $A$ a \FG $R$-module and $B = \bigcap_{n \in \NB} I^nA$.  Then $IB = B$.
\end{theorem}

\begin{proof}
  Since we wish to use~\cref{lem:herstein-krull-intersection-theorem}, we need to ensure the adequate setup.  Let us pick $C$ as maximal submodule \WRT the property $C \cap B = IB$.  As each $I^nA \subseteq A$, it must also hold for their intersection, thus $B \subseteq A$ as well.  Then by~\cref{lem:herstein-krull-intersection-theorem} we have $I^nA \subseteq C$ for some $n \in \NB$.  By hypothesis $B \subseteq I^nA$ and so $B \subseteq C$, which implies
\[
B = B \cap C = IB.
\]
\end{proof}

Combining~\cref{th:krull-intersection-theorem} with either~\cref{th:one-plus-y-B-is-zero} or~\cref{th:the-nakayama-lemma} we obtain useful results about vanishing of modules under multiplication by certain ideals.  The next theorem prepares the ground.

% Theorem 75
\begin{theorem}
  Let $R$ be a ring, $I \subseteq R$ an ideal, $A$ an $R$-module generated by $n$ elements and $x$ an element of $R$ satisfying $xA \subseteq IA$.  Then $(x^n + y)A = 0$ for some $y \in I$.
\end{theorem}

\begin{proof}
Let $A = \langle a_1,\ldots,a_n \rangle$.  By hypothesis we have $xa_i = \sum_j^n y_{ij}a_j$ for $y_{ij} \in I$.  Bringing everything to the left, we get the vector equation
\begin{center}
$M\mathbf{a} =
\begin{pmatrix}
   (x - y_{11}) & -y_{12} & \cdots & -y_{1n} \\
   -y_{21} & (x - y_{22}) & \cdots & -y_{2n} \\
   \vdots  & \vdots  & \ddots & \vdots  \\
   -y_{n1} & -y_{n2} & \cdots & (x - y_{nn})
\end{pmatrix}
\begin{pmatrix}
  a_1 \\
  a_2 \\
  \vdots \\
  a_n
\end{pmatrix}
=
\begin{pmatrix}
  0 \\
  0 \\
  \vdots \\
  0
\end{pmatrix}$
\end{center}
Then by Cramer's Rule we have $(\det M) a_j = 0$ for all $a_j$, and since $A$ is generated by $a_j$ we have $(\det M)A = 0$.  Expanding $\det M$ we get a polynomial of the form $x^n + y$ for some $y \in I$.
\end{proof}

Using $x = 1$ we get the following corollary.

% Theorem 76
\begin{corollary}\label{th:one-plus-y-B-is-zero}
  Let $R$ be a ring, $I \subseteq R$ an ideal, $A$ a \FG $R$-module satisfying $A = IA$.  Then $(1 + y)A = 0$ for some $y \in I$.
\end{corollary}

The familiar notion of zero-divisors in rings can be quite straight-forwardly generalized to the case of modules as follows.

\begin{definition}\index{terms}{zero-divisor}\index{symbols}{$\ZDIV{A}$@set of zero-divisors}
  Let $R$ be a ring, $A$ an $R$-module.  An element $r \in R$ is called a \emph{zero-divisor on} $A$ if the map
\begin{align*}
\varphi_r : A &\rightarrow A \\
               a &\mapsto ra
\end{align*}
is not injective.  A set of all zero-divisors of $A$ will be denoted $\ZDIV{A}$.
\end{definition}

As usual, taking the ring $R$ for the $R$-module $A$ recovers the ``ring'' definition.

We now wish to apply~\cref{th:one-plus-y-B-is-zero} in two situations where we show that $\bigcap_{n \in \NB} I^n A= 0$ for some kinds of ideal $I$.  However, we require an additional assumption regarding the zero-divisors.  In the first result, it will be explicitly required, in the second it will be implicit in the structure of the considered ideal.  The following definition formalizes the first case.

\begin{definition}\index{terms}{module!torsion-free}
  Let $R$ be an \ID, $A$ an $R$-module.  We say that $A$ is \emph{torsion-free} if $ra = 0; r \in R, a \in A$ implies $r = 0$ or $a = 0$.
\end{definition}

% Theorem 77
\begin{theorem}
  Let $R$ be a Noetherian \ID, $I \subsetneq R$ a proper ideal and $A$ \FG torsion-free $R$-module.  Then
\[
\bigcap_{n \in \NB} I^nA = 0.
\]
\end{theorem}

\begin{proof}
  Let $B = \bigcap_{n \in \NB} I^nA$.  By~\cref{th:krull-intersection-theorem}, $B = IB$.  By~\cref{th:one-plus-y-B-is-zero}\footnote{$B$ here plays the role of $A$ in~\cref{th:one-plus-y-B-is-zero}} we have $(1+y)B = 0$ for some $y \in I$.  Because $A$ is torsion-free, either $B = 0$ or $1+y = 0$.  But $y$ can not equal $-1$ because $I$ is proper.
\end{proof}

In the second case, we will consider a special ideal called the \emph{Jacobson radical}.  Intuitively, the Jacobson radical is another collection of ``bad'' elements of the ring (cf. the \emph{nilradical}).  We know that each nilpotent is a zero divisor which is a non-unit.  We can take these implications as defining a sequence of degrees of ``badness''.  Then, being a non-unit is a weaker notion (meaning less bad) than being a zero divisor.

However, the elements of Jacobson radical of ring $R$ satisfy a stronger property than just being non-units: they must not act as a unit in \emph{any} field of the form $R/M$, that is, they must always project onto zero in the quotient.  Incidentally, the field of the form $R/M$ is also a simple $R$-module, which allows for another equivalent characteristic.

\begin{definition}\index{terms}{Jacobson radical}\index{terms}{radical!Jacobson}
  Let $R$ be a ring (not necessarily commutative).  Then the ideal $J$ is called \emph{Jacobson radical} if any (and therefore all) of the following is satisfied:
  \begin{enumerate}
  \item $J$ is the intersection of all maximal left ideals.
  \item $J$ is the intersection of all maximal right ideals.
  \item $J$ annihilates all simple left (right) $R$-modules.
  \end{enumerate}
\end{definition}

\begin{remark}
  One peculiar thing to notice is that this ideal is always two-sided ideal, even if the ring is non-commutative.  It is also useful to observe that it is not necessarily equal to the intersection of all maximal \emph{two-sided} ideals in a non-commutative ring (see Ex. 3.15 in~\cite{lam2001first}).
\end{remark}

Corresponding to the previous sequence of ``bad'' elements, the \emph{nilradical} of a ring (intersection of all prime ideals) is indeed contained\footnote{Here commutativity is required, otherwise, only the nilpotents in the \emph{center} of the ring are under the Jacobson radical.} in the Jacobson radical (intersection of all maximal ideals), which is contained in the union of all maximal ideals.

\begin{remark}
  We can formulate a characterization of the nilradical in terms of annihilation of some $R$-modules, similar to the one for Jacobson radical.  Namely, an element $r \in R$ is nilpotent \IFF it annihilates all {\ID}s of the form $R/P$, where $P$ is a prime ideal of $R$.  While this condition encodes the before mentioned Krull's theorem (15.2.9 in~\cite{dummit1999abstract}), a still more general equivalent condition is available: $r$ is nilpotent \IFF it is a zero-divisor on \emph{any} $R$-module $M$.
\end{remark}

The next theorem provides another characterization of Jacobson radical.

\begin{theorem}[4.3.10 in~\cite{berrick2000introduction}]\label{th:jacobson-ideal-quasiregular}
  Let $R$ be a ring, $J$ its Jacobson radical.  Then for every $x \in J$, $(1 - x)$ is invertible (has a two-sided inverse).
\end{theorem}

There is considerable discord in naming when it comes to the following lemma---it is sometimes stated together with what we introduced as~\cref{th:one-plus-y-B-is-zero}, sometimes with other (equivalent or similar) statements.  We choose to introduce the simplest case (with strongest assumptions).  Using~\cref{th:jacobson-ideal-quasiregular}, it is simple to prove it from~\cref{th:one-plus-y-B-is-zero}\footnote{Because $y$ is in $J$, $(1+y)$ is a unit, hence can not annihilate $A$, therefore $A = 0$.  See also proof of~\cref{th:inf-isec-jac-times-mod-zero}}, but we opt for a more direct proof.

% Theorem 78
\begin{theorem}[The Nakayama lemma]\label{th:the-nakayama-lemma}
  Let $R$ be a ring (not necessarily commutative), let $A$ be a \FG left $R$-module and assume that $JA = A$, where $J$ is the Jacobson radical of $R$.  Then $A = 0$.
\end{theorem}

\begin{proof}
  Let $A = \langle a_1,\ldots,a_n \rangle$, where this is the minimal generating set (\WRT number of generators).  Then
\[
a_1 = j_1a_1 + \ldots + j_na_n;~~~ j_i \in J.
\]
Because $j_1 \in J$, by~\cref{th:jacobson-ideal-quasiregular}, $(1 - j_1)$ is invertible.  Then
\begin{align*}
  a_1 - j_1a_1 &= j_2a_2 + \ldots + j_na_n \\
  (1 - j_1)a_1 &= j_2a_2 + \ldots + j_na_n \\
  a_1 &= (1 - j_1)^{-1}(j_2a_2 + \ldots + j_na_n)
\end{align*}
and $a_1$ can be expressed in terms of $a_2,\ldots,a_n$, a contradiction.
\end{proof}

\begin{theorem}\label{th:inf-isec-jac-times-mod-zero}
  Let $R$ be a commutative \NR with Jacobson radical $J$ and $A$ a \FG $R$-module.  Then $\bigcap_{n \in \NB}J^nA = 0$.
\end{theorem}

\begin{proof}
  Let $B = \bigcap_{n \in \NB}J^nA$.  By~\cref{th:krull-intersection-theorem} we get $B = JB$.  The result is immediate from~\cref{th:the-nakayama-lemma}, but we can also use~\cref{th:one-plus-y-B-is-zero}: considering the same $B$, we get $(1+y)B = 0$ for some $y \in J$.  By~\cref{th:jacobson-ideal-quasiregular}, $(1+y)$ is a unit.  But then it can not be a zero divisor\footnote{If $rx = 0$ and $r$ is a unit, then $x = r^{-1}rx = r^{-1}0 = 0$, and so $x = 0$, making multiplication by $r$ injective.}, hence $B = 0$.
\end{proof}

In the remaining part of the thesis we will describe some basic structure regarding minimal and maximal prime ideals.  We will first look at their structure in a general case, then with the additional assumption of living in a Noetherian ring or module.  Then we will show that Noetherian condition is enough to ensure that there is only a finite number of both maximal and minimal primes in a given ring.

%Before we delve into the theorems, let us state the definitions.

\section{Maximal prime ideals}

Given that any maximal ideal is also prime---considered by set inclusion---the maximal prime ideals would of course be just the maximal ideals of the ring.  To get a more useful notion of maximal prime, we shall consider them \WRT some subset of the ring.  We define them as follows:

\begin{definition}\index{terms}{prime!maximal}\index{terms}{maximal!prime}
  Let $R$ be a (commutative) ring, $A$ any non-zero $R$-module.  The prime ideals maximal in the set of zero-divisors of $A$ (cf.~\cref{example:maximal-primes}) are called the \emph{maximal primes} of $A$.
\end{definition}

%  Further, if $A$ is of the form $R/I$ where $I$ is an ideal in $R$, we say maximal primes of $I$ rather than $R/I$.

The following lemma is an easy consequence of a more general theorem found in~\cite{mccoy} (Theorem 1).  We provide a direct proof.

\begin{remark}
In the following lemma we do not require the subring to contain the element $1_R$ of the bigger ring.
\end{remark}

% Theorem 81
\begin{lemma}[McCoy,~\cite{mccoy}]\label{th:finite-union-of-primes-subring-under}
  Let $R$ be a commutative ring, $I_1, \ldots, I_n$ a finite number of ideals in $R$ and $S$ a subring of $R$ such that
\begin{align}
S \subseteq I_1 \cup I_2 \cup \ldots \cup I_n \label{eq:81:2}
\end{align}
Assume that at least $n - 2$ of the $I_j$'s are prime.  Then $S$ is contained in some $I_j$.
\end{lemma}

\begin{proof}
  Let us proceed by full induction on $n$.  For $n = 1$ the result is trivial.  The proof of case $n = 2$ follows the same pattern as the following general case, without requiring the presence of a prime ideal (hence the $n - 2$ condition in the hypothesis).

% For $n = 2$,  we can assume that $S \nsubseteq I_1$ and $S \nsubseteq I_2$ (otherwise the conclusion holds).  Then there exist $x_1 \in S$ but $x_1 \not\in I_1$ and similarly for $x_2$.  Let $y = x_1 + x_2$, then $y \in S$.  Suppose \WLOG that $y \in I_1$.  Since $x_2 \in I_1 \cup I_2$ and it does not lie in $I_1$, it must be that $x_2 \in I_1$.  Then $y - x_2 = x_1$ and $x_1 \in I_1$, a contradiction.

For the case $n > 2$, assume that the result holds for all $m < n$.  We can further assume that
\begin{align}
S \nsubseteq I_1 \cup \ldots \cup \hat{I}_k \cup \ldots \cup I_n \label{eq:81:1}
\end{align}
for each $k \in \{1,\ldots,n\}$, where the hat denotes omission---this step preserves all the assumptions.  Now we will force a contradiction, showing that $S$ must be under the union of some $n - 1$ of the ideals $I_j$ and the result follows by inductive hypothesis.

Pick, then, for each $k$ an element $x_k \in S$ such that $x_k$ is not in the right side of~\eqref{eq:81:1}.  By~\eqref{eq:81:2} it must lie in $I_k$, while not lying in any other $I_{j\neq k}$.  By assumption at least one of $I_j$ must be prime, for example $I_1$.  Let $y = x_1 + x_2x_3\cdots x_n$.  We will show that $y$ lies in $S$ while not lying in any $I_j$, which will contradict the assumption $S \subseteq \bigcup_i^n I_i$.

Assume first that $y \in I_1$.  Then $y - x_1 \in I_1$ and $x_2x_3\cdots x_n \in I_1$.  But because $I_1$ is prime, some $x_{j \neq 1}$ lies in $I_1$, a contradiction.  Now assume $y \in I_{j \neq 1}$.  Because all $I$s are ideals, the element $x_2x_3\cdots x_n$ lies in all $I_2,\ldots,I_n$.  Then $y - x_2x_3\cdots x_n \in I_j$ and so $x_1 \in I_j$, a contradiction.
\end{proof}

% Theorem 83
% \begin{corollary}
%   Let $R$ be a commutative ring, $S \subseteq R$ a subring and $I \subseteq S \subseteq R$ an ideal.  Suppose that $I \neq S$ and that
% \begin{align}
% S \setminus I \subseteq P_1 \cup \ldots \cup P_n \label{eq:83:1}
% \end{align}
% where $P_i$ are prime ideals in $R$.  Then $S \subseteq P_i$ for some $i$.
% \end{corollary}

% \begin{proof}
%   From~\eqref{eq:83:1} follows that
% \[
% S \subseteq I \cup P_1 \cup \ldots \cup P_n
% \]
% By~\cref{th:finite-union-of-primes-subring-under} $S$ is under $I$ or some $P_i$.  But $S \subseteq I$ is ruled out by assumption.
% \end{proof}

We have already shown in~\cref{example:maximal-primes} that the set of zero-divisors $\ZDIV{A}$ of an $R$-module $A$ is a union of prime ideals.  Following our definition of maximal primes, it is obvious that considering the union of just these we obtain the same set.  It follows than that the set of zero-divisors $\ZDIV{A}$ is the union of maximal primes of the module $A$.  In Noetherian settings, we can obtain even more precise description of these maximal primes, this we capture in the following theorem.

% Theorem 80
\begin{theorem}\label{th:noetherian-module-finite-maximal-primes}
  Let $R$ be a \NR, $A$ a \FG non-zero $R$-module.  Then there are only a finite number of maximal primes of $A$, and each of them is the annihilator of a non-zero element of $A$, that is, of the form $\Ann_R(a)$ for some $a \in A$.
\end{theorem}

\begin{proof}
Consider the set $\mathcal{A}$ of annihilators of non-zero elements of $A$:
\[
\mathcal{A} = \{ \Ann_R(a) \mid a \in A \}.
\]
Because $R$ is Noetherian, each annihilator is contained in a maximal one (\WRT inclusion in $\mathcal{A}$).  By definition every zero-divisor lies in some $\Ann(a)$, therefore the set of zero devisors $\ZDIV{A}$ is the union of these maximal annihilators.  By~\cref{th:maximal-ann-prime}, each of them is prime.

Now we show that there is only finitely many of them.  Denote $P_i$ the maximal annihilator corresponding to $a_i$.  The submodule spanned by all $a_i$s is finitely generated ($A$ is Noetherian), \WLOG we can pick $\langle a_1,\ldots,a_n \rangle$.  Then, for any additional $a_{n+1}$, we have:
\begin{align}
  a_{n+1} &= r_1a_1 + \ldots + r_na_n;~~~r_i \in R. \label{eq:80:1}
\end{align}
Consider $r \in \bigcap_i^n P_i$.  From~\eqref{eq:80:1} we get
\begin{align*}
  ra_{n+1} &= r(r_1a_1 + \ldots + r_na_n) \\
               &= r_1\underbrace{ra_1}_{0} + \ldots + r_n\underbrace{ra_n}_{0} = 0
\end{align*}
hence
\[
P_1 \cap P_2 \cap \ldots \cap P_n \subseteq P_{n+1}.
\]
Now, this implies that some $P_j \subseteq P_{n+1}$.  For if not, we can pick $r_i \in P_i$ such that $r_i \not\in P_{n+1}$ for all $i$.  Let $y = r_1r_2\ldots r_n$.  Then
\[
   ya_{n+1} = r_1ya_1 + \ldots + r_nya_n = 0
\]
and $y \in P_{n+1}$.  Since $P_{n+1}$ is prime, some $r_j \in P_{n+1}$, a contradiction.  So $P_j \subseteq P_{n+1}$.  But this contradicts the maximality of $P_j$ and so there can not be any more $a_i$s.

Let $I$ be any other ideal in $\ZDIV{A} = \bigcup_i^n P_i$.  As every ideal is a subring, by~\cref{th:finite-union-of-primes-subring-under} $I$ lies under some $P_j$, so these are indeed the only maximal primes.
\end{proof}

Now consider a subring $S$ contained in $\ZDIV{A}$.  In general, each element $s \in S$ might require different non-zero element $a_s$ of $A$ to serve as a ``witness'' of the fact that it is indeed a zero-divisor.  However, in the Noetherian case we can take advantage of the above result about the structure of maximal primes and show that a single element of $A$ can serve as a witness for all the elements in $S$.

% Theorem 82
\begin{corollary}
  Let $R$ be a commutative \NR, $A$ \FG non-zero $R$-module and $S$ a subring in $\ZDIV{A}$.  Then there is a non-zero element $a \in A$ such that $Sa = 0$.
\end{corollary}

\begin{proof}
  By~\cref{th:noetherian-module-finite-maximal-primes}, $\ZDIV{A} = \bigcup_i^n P_i; n \in N$, where $P_i$ is the annihilator of some $a_i \in A$.  By~\cref{th:finite-union-of-primes-subring-under}, $S$ is under some $P_i$, therefore ``annihilated'' by $a_i$.
\end{proof}

\section{Minimal prime ideals}

Having described the maximal primes, we move to the other extreme and look at the minimal prime ideals, first in general and then in the Noetherian case.

\begin{definition}\index{terms}{prime!minimal}\index{terms}{minimal!prime}
  Let $R$ be a (commutative) ring, $I \subseteq R$ an ideal.  Then we say $P$ is \emph{minimal prime ideal} over $I$ if $P$ prime ideal minimal (\WRT inclusion) among all prime ideals containing $I$.  \emph{Minimal prime ideals} of the ring are those minimal over the zero ideal.
\end{definition}

Note that the definition does not rule out the possibility that $I$ is minimal prime over itself---that happens precisely when $I$ is prime.  Therefore, in \ID, the only minimal prime is the zero ideal.

% Theorem 84
\begin{theorem}\label{th:minimal-prime-is-under-zdiv}
  Let $R$ be a ring, $A$ a non-zero $R$-module, $I$ the annihilator of $A$ and $P$ a prime ideal in $R$ minimal over $I$.  Then $P \subseteq \ZDIV{A}$.
\end{theorem}

\begin{proof}
We will once again use~\cref{th:maximal-mcs-prime}.  To set it up, we need to find a passing \MCS.  Let $S$ denote all elements of the form $pq$, where $p \not\in P$ and $q \not\in \ZDIV{A}$.  From commutativity, $S$ is closed (we can rearrange $pq \cdot p'q'$ as $(pp')(qq')$ to get the required form).

Next we show that $S$ is disjoined from $I$.  Suppose that for some $pq \in S$ we would have $pqA = 0$.  Then we must have $pA = 0$ since $q$ is not a zero-divisor.  But then $p \in I \subseteq P$, a contradiction.  Now we enlarge $I$ to an ideal $Q$ maximal \WRT exclusion of $S$.  By~\cref{th:maximal-mcs-prime} $Q$ is prime.

By the way $S$ was defined, we have assured that $Q \subseteq P$ and $Q \subseteq \ZDIV{A}$.  Suppose, for instance, that $p \in Q$ and $p \not\in P$.  Then pick any $q \not\in \ZDIV{A}$\footnote{Such an element must exist, for otherwise $RA = 0 \Rightarrow 1A = 0$ and $A = 0$, contradicting $A$ is non-zero.}.  Since $Q$ is an ideal, $pq \in Q$, and $pq \in S$ by definition.  But this contradicts the fact that $Q$ is disjoint from $S$.  The argument for $Q \subseteq \ZDIV{A}$ is analogical.  Finally, by construction, $I \subseteq Q$ and by minimality of $P$ we have $Q = P$.
\end{proof}

An immediate corollary of the above is a useful theorem about the minimal prime ideals of a ring and their relation to zero-divisors.

\begin{corollary}
  Let $R$ be a (unital) ring.  Then the union of the minimal prime ideals of $R$ is under $\ZDIV{R}$.
\end{corollary}

\begin{proof}
  It is elementary to show that $\Ann_R(R) = 0$.\footnote{Let $r$ be another annihilator, then $0 = 1r = r$.}  By~\cref{th:minimal-prime-is-under-zdiv} the minimal primes over $0$---these are the minimal primes of $R$ by definition---are under $\ZDIV{R}$, and so must be their union.
\end{proof}

\begin{definition}
  TODO: Insert definition of localization here.
\end{definition}

We will now look what happens if we move into a Noetherian module.  To prepare the ground, we will prove a simple (albeit a bit technical) lemma concerning ``vanishing'' of localizations.
%containing the annihilator of $A$
\begin{lemma}\label{lem:localization-zero-iff-sA-is-zero}
  Let $R$ be a ring, $P \subseteq R$ a prime ideal, $I \subseteq R$ any ideal and $A$ a \FG $R$-module.  Then $I_PA_P = 0$ \IFF $sIA = 0$ for some $s \not\in P$.
\end{lemma}

\begin{proof}
Let $S = R \setminus P$.

If $sIA = 0$ for some $s \in S$, it means that either $s \in \Ann(IA)$ or $IA = 0$.  In case $IA = 0$, we are done.  Suppose $s \in \Ann(IA)$.  Then
\[
\frac{i}{t_1}\cdot\frac{a}{t_2} = \frac{ia}{t_1t_2} \sim \frac{s(ia)}{st_1t_2} \sim \frac{0}{st_1t_2}; ~~~ t_1,t_2 \in S
\]
and so every element of $I_PA_P$ is congruent to zero.

Conversely, let $I_PA_P = 0$.  Because both $A$ and $R$ are Noetherian, $IA$ can also be finitely generated.  Namely, if the generators of $I$ are $i_1,\ldots,i_m$, and the generators of $A$ are $a_1,\ldots,a_n$, we can express any $ia \in IA$ as a weighted sum of products $i_ka_l$, written in matrix form as $\mathbf{i}M\mathbf{a}^{\mathrm{T}}$, where $M$ is a matrix of coefficients over $R$.  Then $I_PA_P$ is also finitely generated, let's say by $\frac{i_1a_1}{1},\ldots,\frac{i_ma_n}{1}$.  Then in particular
\[
\frac{i_ka_l}{1} \sim \frac{0}{1} \Leftrightarrow ~ u_{kl}(i_ka_l) = 0 ; ~~~ u_{kl} \in S
\]
Let $s = u_{11}u_{12}\cdots u_{21}u_{22} \cdots u_{mn}$.  It is clear that $s$ annihilates all the generators of $IA$, hence $sIA = 0$.
\end{proof}

\begin{corollary}\label{cor:localization-zero-iff-sA-is-zero}
  As a special case, let $I = R$.  Then $A_P = 0$ \IFF $sA = 0$ for some $s \not\in P$.
\end{corollary}

Now comes the promised specialized version of~\cref{th:minimal-prime-is-under-zdiv}.

% 84: Let $A$ be a non-zero $R$-module, $I$ the annihilator of $A$ and $P$ a prime ideal in $R$ minimal over $I$.  Then $P \subseteq \ZDIV{A}$.
% Theorem 86
\begin{theorem}
  Let $R$ be Noetherian and let $A$ be a \FG non-zero $R$-module with annihilator $I$.  Let $P$ be a prime ideal in $R$ minimal over $I$.  Then $P$ is the annihilator of a non-zero element of $A$, i.e. $P = \Ann_R(a)$ for some $a \in A$.
\end{theorem}

\begin{proof}
Let $S = R \setminus P$ in all the following.

We will pass to the localizations of $A$ and $R$ at $P$.  Note that $R_P$ is again Noetherian and $A_P$ finitely generated.  During localizations it might happen that $A_P$ will be reduced to zero module.  By~\cref{cor:localization-zero-iff-sA-is-zero}, that can happen only if $sA = 0$ for some $s \in S$.  But that would mean $s \in \Ann(A) \subseteq P$, and $S \cap \Ann(A) = \emptyset$, so $A_P$ must not be zero.

Next we show that $P_P$ is minimal over $I_P$ (which is the annihilator of $A_P$).  Suppose for contradiction that there is a prime ideal $T \subseteq R_P$ such that $I_P \subseteq T \subsetneq P_P$.  This ideal must be of the form $Q_P$, where $Q \subsetneq P$ is prime ideal in $R$.  Now we show that $I \subseteq Q$ which will contradict the assumption that $P$ is the minimal ideal over $I$.

To show that $I \subseteq Q$, take any $i \in I$.  Then $\frac{i}{1} \in I_P \subseteq Q_P$ and:
\begin{align}
\frac{i}{1} \sim \frac{q}{s} \Leftrightarrow s'(si - q) = 0 \Leftrightarrow s'si = s'q \label{eq:86:1}
\end{align}
for suitable $q \in Q; s,s' \in S$ (i.e. $s,s' \not\in P$).  Since $q \in Q$, also $s'q \in Q$ and so, by~\eqref{eq:86:1}, $s'si \in Q$.  Because $s,s' \not\in P$ and $Q \subsetneq P$, we get $s,s' \not\in Q$.  But $Q$ is prime, so then $i \in Q$.

With $P_P$ being minimal over $I_P$ we can use~\cref{th:minimal-prime-is-under-zdiv} to get $P_P \subseteq \ZDIV{A_P}$.  Because $R_P$ is local ring with the unique maximal ideal $P_P$, it must also be a maximal prime.  In other words, there can not be any ideal above it, because it is maximal, and it is contained in $\ZDIV{A_P}$ by the previous.  By~\cref{th:noetherian-module-finite-maximal-primes}, we get that $P_P = \Ann_{R_P}(\frac{a}{1})$.  Let $B = \langle a \rangle$, the module generated by $a$.  Because $P_P$ annihilates $\frac{a}{1}$, $P_PB_P = 0$.  By~\cref{lem:localization-zero-iff-sA-is-zero} there is some $s \in S$ such that $sP\langle a\rangle = Ps\langle a \rangle = 0$.  Now take $b = sa$, we claim that $P$ is its annihilator.  One inclusion is clear, so let $rb = 0$.  Then $\frac{r}{1}$ annihilates $\frac{a}{1}$, so
\[
\frac{r}{1} \sim \frac{rt}{t} \in P_P
\]
and $rt \in P$.  Because $t \not\in P$, we get $r \in P$.  This proves the theorem.
\end{proof}

We shall not pursue the exposition of \emph{structure} of maximal and minimal primes further.  We have shown that in the Noetherian case they both take the form of annihilators of single non-zero elements of $A$, and in general case are at least quite intimately connected to the set of zero-divisors of the module (see~\cref{example:maximal-primes}, which is a corollary of~\cref{th:saturated-complement-prime} and~\cref{th:minimal-prime-is-under-zdiv}).

We shall finish the chapter with an analogy of~\cref{th:noetherian-module-finite-maximal-primes} regarding the \emph{number} of minimal primes.  We now know that their structure is the same as that of the maximal primes, and we will further show that in the Noetherian case there is also only a finite number of them over any ideal $I$---in particular, $R$ has only a finite number of minimal prime ideals.

We remark that in the following two theorems a weaker condition is required, namely the ACC need only be satisfied on the \emph{radical} ideals.  Of course, any Noetherian ring satisfies this condition, as the ACC holds for \emph{all} ideals there.

% Theorem 87
\begin{theorem}\label{th:radical-is-finite-intersection-of-primes}
  Let $R$ be a commutative ring satisfying the ACC on radical ideals.  Then any radical ideal in $R$ is the intersection of a finite number of prime ideals.
\end{theorem}

\begin{proof}
  We will proceed by contradiction.  Let $\mathcal{I}$ be the set of all radical ideals which are not intersections of a finite number of prime ideals.  We can pick a maximal ideal $I$ from this set thanks to ACC on radical ideals.  This ideal $I$ can not be prime, for otherwise it would be the ``intersection'' of just itself, thus satisfying the assumption.  Therefore, there is $ab \in I$ such that $a \not\in I, b \not\in I$.  Let $J = \sqrt{(I,a)}, K = \sqrt{(I,b)}$.  By maximality of $I$, both $J$ and $K$ can be expressed as finite intersection of prime ideals.  We will show that $I = J \cap K$, reaching a contradiction.

That $I \subseteq J \cap K$ is obvious from the definition (indeed, both $J$ and $K$ contain $I$).  To show the converse, take $x \in J \cap K$.  Since $J$ is radical of $(I,a)$, we get that some power of $x$ lies in $(I,a)$, and similarly for $K$, say
\begin{alignat*}{5}
  & x^m && \in (I,a);~ &&x^m &&= i_1 + ya; \quad && i_1 \in I, y \in R \\
  & x^n && \in (I,b);~ &&x^n &&= i_2 + zb; \quad && i_2 \in I, z \in R
\end{alignat*}
Then
\[
x^{(m+n)} = x^mx^n = i_1i_2 + i_1zb + i_2ya + yzab \in I.
\]
Now $x \in I$, because $I$ is radical, and the theorem is proved.
\end{proof}

In the previous, we can make the ``intersection representation'' unique in the following sense.  Let $I$ be the radical ideal from previous theorem and $P$ any prime above it.  By~\cref{th:minimal-prime-exist}, we know that $P$ contains a prime minimal over $I$.  Therefore, it suffices to take the minimal primes contained in the primes from the intersection.  We will collect this thought in the following theorem.

\begin{remark}
  To prove the next theorem, we will use the fact that prime ideal $P$ contains $I$ \IFF it contains $\sqrt{I}$---the radical of $I$.  We leave the routine verification of this fact as a small exercise.
\end{remark}

% Theorem 88
\begin{theorem}
  Let $R$ be a commutative ring satisfying the ACC on radical ideals, let $I \subseteq R$ be an ideal.  Then there is only a finite number of minimal prime ideals over $I$.
\end{theorem}

\begin{proof}
  By the preceding remark, the situation $I \subseteq P \subseteq \sqrt{I}$ is impossible.  Therefore, minimal primes over $I$ are the same as those over $\sqrt{I}$.  By~\cref{th:radical-is-finite-intersection-of-primes} we know that $\sqrt{I} = \bigcap_i^n P_i$ for some finite $n$, where each $P_i$ is prime.  Now let $P$ be a minimal prime over $\sqrt{I}$.  Then
\[
\sqrt{I} = P_1 \cap P_2 \cap \ldots \cap P_n \subseteq P.
\]
This implies that some $P_j \subseteq P$.\footnote{For suppose there is $p_i \in P_i; p_i \not\in P$ for every $i$.  Then $y = p_1p_2\cdots p_n \in P_i$ for every $i$, therefore $y$ is in the intersection under $P$ and some $p_j \in P$ ($P$ is prime), a contradiction.}  Because $P$ is minimal, we get $P_j = P$.  Therefore all minimal primes are contained in the set $\{P_i\}_i^n$ which is finite.
\end{proof}
% http://math.stackexchange.com/questions/525640/how-to-prove-a-commutative-with-unit-noetherian-ring-a-only-has-finitely-man



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: