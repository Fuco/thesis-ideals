\usepackage[utf8]{inputenc} % Windows OS encoding
\usepackage[T1]{fontenc}
\usepackage[english]{babel} % package for multilingual support

\thesistitle{Ideals in Commutative Rings}
\thesissubtitle{Bachelor thesis}
\thesisstudent{Matúš Goljer}
\thesiswoman{false}
\thesislang{en}
\thesisfaculty{sci}
\thesisyear{2014}
\thesisadvisor{Mgr. Michal Bulant, Ph.D.}

\usepackage{amsmath}
\allowdisplaybreaks
\usepackage{mathtools}

\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{listings}
\usepackage{xifthen}
\usepackage{xspace}

%\usepackage{makeidx}
\usepackage{multind}
\makeindex{terms}
\makeindex{symbols}

\usepackage[toc]{appendix}

\usepackage{titlesec}
\titleformat{\chapter}[display]
{\vspace{-3cm}\bfseries\LARGE}
{\large\chaptertitlename \ \thechapter}
{0pt}
{}

\titleformat{\subsection}{\bfseries}{\thesubsection \hspace{6pt}}{0pt}{}

\makeatletter
\setlength{\@fptop}{0pt}
\makeatother

\usepackage{enumitem}
\setlist[itemize]{leftmargin=20pt,itemindent=0pt}
\setlist[enumerate]{label=(\roman*),parsep=.5em}

\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{matrix}
% \usetikzlibrary{chains}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
% \usetikzlibrary{shapes.arrows}
% \usepackage{etoolbox}

\usepackage{lmodern}
% \usepackage{times}
% \usepackage{charter}
% \usepackage[expert]{mathdesign}
% \usepackage[textlf]{Minion Pro}
% \usefont{T1}{lmr}{m}{n}
\usepackage[activate={true,nocompatibility},final,
tracking=true,kerning=true,spacing=true,
factor=1100,stretch=10,shrink=10]{microtype}
\SetTracking{encoding={*}, shape=sc, size={normalsize, small}}{-20}

\usepackage{setspace}
\usepackage{changepage}
\usepackage{parskip}
\setlength\parskip{10pt plus 3pt minus 3pt}

%% parskip sets topsep to zero, this does something :D
\makeatletter
\def\thm@space@setup{%
  \thm@preskip = \parskip \thm@postskip=0pt
}
\makeatother

\setcounter{tocdepth}{3}
\numberwithin{equation}{chapter}

%========================================
% new commands

\renewcommand{\restriction}{\mathord{\upharpoonright}}

\DeclareMathOperator{\Ann}{Ann}

\newcommand{\IFF}{if and only if\xspace}
\newcommand{\WRT}{with respect to\xspace}
\newcommand{\MCS}{multiplicatively closed set\xspace}
\newcommand{\ID}{integral domain\xspace}
\newcommand{\QF}{field of fractions\xspace}
\newcommand{\QFs}{fields of fractions\xspace}
\newcommand{\NR}{Noetherian ring\xspace}
\newcommand{\FG}{finitely generated\xspace}
\newcommand{\WLOG}{without loss of generality\xspace}

\newcommand{\NB}{\mathbb{N}}
\newcommand{\RB}{\mathbb{R}}
\newcommand{\QB}{\mathbb{Q}}
\newcommand{\ZB}{\mathbb{Z}}

\newcommand{\aF}{\mathbf{a}}

\newcommand{\LOC}[1]{\mathcal{Z}(#1)\xspace} % Zero locus
\newcommand{\VID}[1]{\mathcal{I}(#1)\xspace} % Vanishing ideal
\newcommand{\MOD}[1]{\overline{#1}\xspace}
\newcommand{\ZDIV}[1]{\mathcal{Z}(#1)\xspace}

\newcommand{\units}[1]{#1^\times}
\newcommand{\divides}{\,|\,}
\newcommand{\ALG}[1]{\mbox{$#1$-algebra}\xspace}
\newcommand{\dtext}[1]{\text{~~\{#1\}}}

\newcommand{\polynom}[3]{%
#2_{#3}#1^{#3} + #2_{#3-1}#1^{#3-1} + \ldots + #2_1#1 + #2_0\xspace
}

\newcommand{\polynomi}[3]{%
#1^{#3} + #2_{#3-1}#1^{#3-1} + \ldots + #2_1#1 + #2_0\xspace
}


%========================================
% new enviroments

\newenvironment{diagram}{%
\begin{center}
\begin{tikzpicture}
}{%
\end{tikzpicture}
\end{center}
}

\newenvironment{derivation}{%
  \bgroup
  \vspace{6pt plus 2pt minus 2pt}
  \csname spreadlines\endcsname{9pt}
  \csname flalign*\endcsname\makebox[0.5in][r]{\phantom{.}}
}{%
  \csname endflalign*\endcsname
  \csname endspreadlines\endcsname
  \egroup
}


%========================================
% new floats

\usepackage[hypertexnames=false,hyperfootnotes=false]{hyperref}
\urlstyle{same}
\usepackage[all]{hypcap}
\newcommand*{\Appendixautorefname}{Appendix}

\def\ISOF{\hyperref[app:th:1ISO]{First Isomorphism Theorem}\xspace}
\def\ISOS{\hyperref[app:th:2ISO]{Second Isomorphism Theorem}\xspace}
\def\ISOT{\hyperref[app:th:3ISO]{Third Isomorphism Theorem}\xspace}
\def\ISOL{\hyperref[app:th:4ISO]{Lattice Isomorphism Theorem}\xspace}


%========================================
% theorem definitions

\usepackage{cleveref} %better references

\theoremstyle{definition}
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}{Definition}
\newtheorem{example}[theorem]{Example} %{\mdseries\itshape Example}

\crefname{lemma}{Lemma}{Lemmas}
\crefname{theorem}{Theorem}{Theorems}
\crefname{corollary}{Corollary}{Corollaries}
\crefname{example}{Example}{Examples}

\renewenvironment{proof}[1][Proof]{\begin{trivlist}
\item[\hskip \labelsep {\itshape #1.}]}{\qed\end{trivlist}}
% \renewenvironment{example}[1][Example]{\begin{trivlist}
% \item[\hskip \labelsep {\itshape #1.}]}{\end{trivlist}}
\newenvironment{remark}[1][Remark]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1.}]}{\end{trivlist}}
\renewenvironment{definition}[1][Definition]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1.}]}{\end{trivlist}}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: